#include "LiquidCrystal_attiny.h"
#include  <TinyWireM.h>
#include "tiny_IRremote.h"

//LCD configuration
LiquidCrystal_I2C lcd(0x27,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display
#define BMP180_ADDRESS 0x77  // I2C address of BMP180   
//IR configuration
#define IR_RECV_PIN 3
#define IR_SEND_PIN 4
IRrecv irReciever(IR_RECV_PIN);

//this routine context definition
//TODO: move to its own file
class Context {
  private:
    int cont;
    bool newData;
    bool reading;
  public:
    Context(){
      cont = 0;
      newData = true;
      reading = true;
    }
    decode_results results;
    bool thereIsDataToDisplay(){
      return newData;
    }
    void readDisplayed(){
      newData = false;
    }
    void newRead(){
      newData = true;
      reading = false;
    }
    bool isReading(){
      return reading;
    }
    void resumeReading() {
      reading=true;
    }
    int getCont(){
      cont ++;
      return cont;
    }
};

Context cnx = Context();

Context updateDisplay(Context cnx){
  if(cnx.thereIsDataToDisplay()){
    int cont = cnx.getCont();
    lcd.clear();
    lcd.home();
    lcd.print(cont);
    lcd.print(" len");
    lcd.print(cnx.results.rawlen);
    lcd.print(" prot");
    switch (cnx.results.decode_type)
    {
    case NEC:
      lcd.print("NEC");
      break;
    case SONY:
      lcd.print("SONY");
      break;
    case RC6:
      lcd.print("RC6");
      break;
    case RC5:
      lcd.print("RC5");
      break;
    default:
      lcd.print("???");
      break;
    }
    lcd.setCursor(0,1);
    lcd.print(cnx.results.value);
    cnx.readDisplayed();
  }
  return cnx;
}

Context irRead(Context cnx){
  if(irReciever.decode( &cnx.results)){
    digitalWrite(IR_SEND_PIN, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(100);   
    digitalWrite(IR_SEND_PIN, LOW); 
    cnx.newRead();
    
    irReciever.resume();
  }
  return cnx;
}

void setup() {
  // Now set up the LCD
  //lcd.begin(16,2);               // initialize the lcd
  lcd.init();
  lcd.backlight();
  lcd.home ();                   // go home
  lcd.print("InfraRed Probe");
  lcd.setCursor ( 0, 1 ); // go to position
  lcd.print(" -Ecnil-");
  irReciever.enableIRIn();
  lcd.clear();
}


void loop() {
  cnx = irRead(cnx);
  cnx = updateDisplay(cnx); 
  //if(cnx.isReading()){
  //  cnx.resumeReading();
  //}
}

